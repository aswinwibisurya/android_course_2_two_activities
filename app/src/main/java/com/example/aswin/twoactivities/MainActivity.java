package com.example.aswin.twoactivities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView tvReplyMessage;
    EditText txtMessage;

    public static final int REQUEST_CODE_REPLY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtMessage = findViewById(R.id.txtMessage);
        tvReplyMessage = findViewById(R.id.tvReplyMessage);
    }

    public void sendMessage(View view) {
        String message = txtMessage.getText().toString();

        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("message", message);

        //not expecting result
//        startActivity(intent);

        startActivityForResult(intent, REQUEST_CODE_REPLY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == REQUEST_CODE_REPLY
            && resultCode == Activity.RESULT_OK) {
            String replyMessage = data.getStringExtra("replyMessage");

            tvReplyMessage.setText(replyMessage);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
